package com.daoImpl;

import java.io.Reader;
import java.sql.SQLException;
import java.util.List;

import com.dao.StudentDao;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.model.Student;

public class StudentDaoImpl implements StudentDao {

	private static SqlMapClient sqlMapClient = null;
	static {
		try {
			Reader reader = com.ibatis.common.resources.Resources
					.getResourceAsReader("com/ibatis/config/SqlMapConfig.xml");
			sqlMapClient = com.ibatis.sqlmap.client.SqlMapClientBuilder
					.buildSqlMapClient(reader);
			reader.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void addStudent(Student student) {
		// TODO Auto-generated method stub
		try {
			sqlMapClient.queryForList("selectAllStudent");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void addStudentBySequence(Student student) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteStudentById(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateStudentById(Student student) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Student> queryAllStudent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Student> queryStudentByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Student queryStudentById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	public static void main(String[] args) {
		List<Student> allStudent = new StudentDaoImpl().queryAllStudent();
		for (Student stu : allStudent) {
			System.out.println(stu.getSname());
		}
	}
}
