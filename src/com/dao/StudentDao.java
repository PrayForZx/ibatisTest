package com.dao;

import java.util.List;

import com.model.Student;

public interface StudentDao {
	public void addStudent(Student student);

	public void addStudentBySequence(Student student);

	public void deleteStudentById(int id);

	public void updateStudentById(Student student);

	public List<Student> queryAllStudent();

	public List<Student> queryStudentByName(String name);

	public Student queryStudentById(int id);
}
